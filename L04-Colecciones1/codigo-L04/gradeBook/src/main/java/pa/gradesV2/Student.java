/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa.gradesV2;

/**
 *
 * @author pa
 */
public class Student {
    
    private int grade;
    private String name;
    
    public Student(String nombre){
    grade= 0;
    name= nombre;
    }
    
    public String getName(){
    return name;
    }
    
    public int getGrade(){
    return grade;
    }
     
    public void setGrade(int nota){
    grade= nota;
    }
}
