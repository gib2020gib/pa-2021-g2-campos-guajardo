package pa;

/**
 * This class represents a simple picture. You can draw the picture using
 * the draw method. But wait, there's more: being an electronic picture, it
 * can be changed. You can set it to black-and-white display and back to
 * colors (only after it's been drawn, of course).
 *
 * This class was written as an early example for teaching Java with BlueJ.
 * 
 * @author  Michael K�lling and David J. Barnes
 * @version 2016.02.29
 */
public class Picture
{
    private Square wall;
    private Square window;
    private Triangle roof;
    private Circle sun;
    private boolean drawn;
    private Triangle triangulo2; 
    private Person persona ;

    /**
     * Constructor for objects of class Picture
     */
    public Picture()
    {
        wall = new Square();
        window = new Square();
        roof = new Triangle();  
        sun = new Circle();
        triangulo2 = new Triangle();
        persona = new Person();
        drawn = false;
        
        
    }

    /**
     * Draw this picture.
     */
    public void draw()
    {
        if(!drawn) {
            wall.moveHorizontal(50);
            wall.moveVertical(70);
            wall.changeSize(70);
            wall.makeVisible();
            wall.changeColor("green");
            
            window.changeColor("blue");
            window.moveHorizontal(-200);
            window.moveVertical(70);
            window.changeSize(70);
            window.makeVisible();
    
            roof.changeSize(60,70);
            roof.changeColor("red");
            roof.moveHorizontal(180);
            roof.moveVertical(-10);
            roof.makeVisible();
    
            sun.changeColor("yellow");
            sun.moveHorizontal(0);
            sun.moveVertical(-40);
            sun.changeSize(80);
            sun.makeVisible();
            drawn = true;
         

            persona.makeVisible();
            persona.moveVertical(20);
            persona.moveHorizontal(-20);



            triangulo2.changeColor("red");
            triangulo2.makeVisible();
            triangulo2.moveHorizontal(-70);
            triangulo2.moveVertical(-10);
        
        }
    }

    /**
     * Change this picture to black/white display
     */
    public void setBlackAndWhite()
    {
        wall.changeColor("black");
        window.changeColor("white");
        roof.changeColor("black");
        sun.changeColor("black");
    }

    /**
     * Change this picture to use color display
     */
    public void setColor()
    {
        wall.changeColor("red");
        window.changeColor("black");
        roof.changeColor("green");
        sun.changeColor("yellow");
    }
}
