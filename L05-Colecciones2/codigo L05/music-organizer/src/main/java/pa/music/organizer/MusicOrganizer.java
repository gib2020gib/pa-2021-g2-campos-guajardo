package pa.music.organizer;

import java.util.ArrayList;
import pa.music.player.MusicPlayer;

/**
 * A class to hold details of audio fileNames. It includes a player
 * to play the audio files.
 * 
 * @author David J. Barnes and Michael Kölling
 * @version 2016.02.29
 */
public class MusicOrganizer {
    /**
     * An ArrayList for storing the music fileNames
     * The fileName includes a file extension and
     * directory path file, i.e. "/src/main/resources/title1.mp3"
     */
    private ArrayList<String> fileNames;
    /**
     * An ArrayList for storing the titles of music fileNames
     * (extension file and path file are not included)
     */
    private ArrayList<String> titles;
    /**
     * A player for the music fileNames
     */
    private MusicPlayer player;
        
    /**
     * Create a MusicOrganizer
     */
    public MusicOrganizer() {
        fileNames = new ArrayList<>();
        player = new MusicPlayer();
        titles = new ArrayList<>();
    }
    
  
    /**
     * Return the number of fileNames in the collection.
     * @return The number of fileNames in the collection.
     */
    public int getNumberOfFiles() {
        return fileNames.size();
    }
    
    
    /**
     * Return an ArrayList with the fileNames in the collection.
     * @return An ArrayList with the fileNames in the collection.
     */    
    public ArrayList<String> getFiles() {
        return fileNames;
    }
    
    
    
    /**
     * Start playing a file in the collection.
     * Plays all frames of file (unless stopPlaying() is used)
     * @param index The index of the file to be played.
     */
    public void startPlaying(int index) {
        if(validIndex(index)) {
            String filename = fileNames.get(index);
            player.playFile(filename);
        }
    }

    /**
     * Stop the player.
     */
    public void stopPlaying() {
        player.stop();
    }

    /**
     * Play a file in the collection. Only plays a sample (first 500 frames of
     * the file). Then stops.
     * @param index The index of the file to be played.
     */
    public void playAndWait(int index) {
        if(validIndex(index)) {
            String filename = fileNames.get(index);
            player.playSample(filename);
        }
    }
    
    /**
     * Play a file in the collection. Only plays a sample (first numFrames frames
     * of the file given as a parameter). Then stops.
     * @param index The index of the file to be played.
     * @param numFrames The number of frames to be played
     */
    public void playAndWait(int index, int numFrames) {
        if(validIndex(index)) {
            String filename = fileNames.get(index);
            player.playSample(filename,numFrames);
        }
    }
    
    
    /**
     * Initialize the fileNames and titles of our music organizer
     * with an array of titles.
     * <p>
     * clears fileNames and titles fields.
     * Then adds title names, and file names to fileNames and titles fields,
     * respectively.
     * </p>
     * <p>
     * A file name is created adding to a title the preffix 
     * "src/main/resources/mp3/", and the suffix ".mp3".
     * Then, each file name is added to fileNames field
     * </p>
     * @param listaTemas Array of Strings with title names
     * 
     */
    public void inicializarConMp3(String[] listaTemas) {
        titles.clear(); 
        fileNames.clear(); 
        
        for (int i=0; i<listaTemas.length; i++){
            titles.add(listaTemas[i]);
            fileNames.add("src/main/resources/mp3" + listaTemas[i] + ".mp3");
        }
    }
    
    /**
     * Determine whether the given index is valid for the collection.
     * Print an error message if it is not:
     * - "Index cannot be negative: ", when the index is negative,
     * - "Index is too large: ", when the index is > that colleccion size
     * 
     * @param index The index to be checked.
     * @return true if the index is valid, false otherwise.
     */
    private boolean validIndex(int index) {
        boolean valid = false;
        
        //si el indice esta entre 0 y -1 
        if (index >=0 && index< sileName.size){
              valid true;
    }
        return false;
    }
    
    public void listFile (int index){
        if (validIndex(index) == true){
            System.out.println(fileNames.get(index));}
    }
    
    public void listAllFileNames(){
        
    for(int i=0; i< fileNames.size(); i++){
        System.out.println("%d. %s\n" , i , fileNames.get(i));
    }
    }
    
    public void listAllTitleNames(){
        
    for(int i=0; i< titles.size(); i++){
        System.out.println("%d. %s\n" , i , titles.get(i));
    }
    }
    
    public int findFirst(String searchString){
        int pos;
        
        pos = -1;
        for (int i= 0; i < titles.size() && pos ==-1; i++){
        if (titles.get(i).equals(searchString)){
            pos =i;
            
        }}
        return pos; 
    }
    
    public void addTitle(String title){
        int pos;
        pos= findFirst (newTitle);
        if (pos==-1){
            titles.add(newTitle);
            fileNames.add("src/main/resources/mp3" + newTitle[i] + ".mp3");
        }
        
    }
    
     public void removeTitle(int index){
         if (validIndex(index)){
         titles.remove(index);
         fileNames.remove(index);
           }
         
    public void remove (String subcadena){
        
        String n= "ANA";
        if (n.contains("os")); 
    }
        
    }

}
