/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pa.gui;

import javax.swing.*;
import java.awt.*;  
import pa.music.organizer.MusicOrganizer;

/**
 *
 * @author eli
 */
public class EntornoGrafico {
    private String[] listaTemas;
    private MusicOrganizer organizadorMusica;
    private boolean playing;  
    
    public EntornoGrafico (String[] listaTemas) {
        
        this.listaTemas = listaTemas;
        organizadorMusica = new MusicOrganizer();              
        System.out.println("Cargando las canciones en el reproductor...");        
        //organizadorMusica.inicializarConMp3(listaTemas);
        playing = false; 
    }
  
    //public static void main(String args[]) {    
    public void startAplicacion () { 
        
        // Creamos el Marco        
        JFrame frame = new JFrame("Reproductor mp3");       
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);       
        //frame.setSize(400, 400); 
        frame.setSize(600, 400);
  
        
        // Creamos el panel de la parte inferior con los botones
        JPanel panelBotones = new JPanel();             
        JButton play = new JButton("Play");       
        JButton stop = new JButton("Stop");
        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> {
         frame.dispose();
        });
        // Componentes agregados usando Flow Layout
        panelBotones.add(play);       
        panelBotones.add(stop);
        panelBotones.add(exit);        
        
        //Cremos el panel superior con el texto y el menú desplegable
        JPanel panelCanciones = new JPanel();
        JLabel texto = new JLabel("Selecciona la canción: "); 
        
        JComboBox listaCanciones = new JComboBox(listaTemas);
        listaCanciones.setSelectedIndex(4);
        panelCanciones.add(texto);
        panelCanciones.add(listaCanciones);
        
        // Área de texto en el centro con un scrollpane
        JTextArea ta = new JTextArea(); 
        ta.append(" Bienvenido");
        ta.setEditable(false);
        ta.append("\n Puede escuchar "+ listaTemas.length+ " temas");
        
        JScrollPane scrollpane = new JScrollPane(ta);
        
        // Agregamos todos los componentes al marco.      
        frame.getContentPane().add(BorderLayout.SOUTH, panelBotones);       
        frame.getContentPane().add(BorderLayout.CENTER, scrollpane);  
        frame.getContentPane().add(BorderLayout.NORTH, panelCanciones);             
        frame.setVisible(true);  

         
        //añadimos la funcionalidad del botón play
        play.addActionListener(e -> {
         System.out.println("Botón play habilitado = "+play.isEnabled());
         if (play.isEnabled()) {
            play.setEnabled(false);
            System.out.println("Botón play habilitado = "+play.isEnabled());
            
            String cancion = (String)listaCanciones.getSelectedItem();
            if (!playing) {
                ta.append("\n Escuchando el tema: "+ cancion+ " ..."); 
                organizadorMusica.startPlaying(listaCanciones.getSelectedIndex()); 
                playing= true;
            } else ta.append("\n Reproductor ocupado");
                       
            play.setEnabled(true);
         }
        });
  
        //añadimos la funcionalidad del botón stop
        stop.addActionListener(e -> {
         if (play.isEnabled()) {
           organizadorMusica.stopPlaying();
           System.out.println("Reproductor detenido");
           ta.append("\n Reproductor detenido"); 
           playing=false;
         } 
        });    
    }
}
