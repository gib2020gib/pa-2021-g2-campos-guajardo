package pa;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pa
 */
public class Prueba {
    public static void main(String args []){
        Book book1 = new Book ("Juego de Tronos", "Gorge RR Martin", 1300 );
        Book book2 = new Book ("El nombre de la Rosa", "Umberto Eco", 987);
        Book book3 = new Book ("Tutankamon" , "Chistian Jacq",876);
        
        book1.setRefNumber("33");
        book2.setRefNumber("333");
        book3.setRefNumber("3333");
        
        book1.printDetails();
        book2.printDetails();
        book3.printDetails();
        
        book1.setRefNumber("555");
        book2.setRefNumber("678");
        
        book1.borrow();
        book1.borrow();
        book2.borrow();
        book1.printDetails();
        book2.printDetails();
        
    }
    
    
}
