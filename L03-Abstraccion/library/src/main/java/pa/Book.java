package pa;
/**
 * A class that maintains information on a book.
 * This might form part of a larger application such
 * as a library system, for instance.
 *
 * @author (Insert your name here.)
 * @version (Insert today's date here.)
 */
public class Book
{
    // The fields.
    private String author;
    private String title;
    private int pages;
    private String refNumber;
    
    private int borrowed ;

    /**
     * Set the author and title fields when this object
     * is constructed.
     */
    public Book(String bookAuthor, String bookTitle, int bookPages)
    {
        author = bookAuthor;
        title = bookTitle;
        pages = bookPages;
        refNumber = "";
        borrowed = 0;     
        
      
    }
    public String getTitle(){
    return title;
    }
    
    public int getPages(){
        return pages;
    }
    
    public void printDetails(){
        System.out.println("Author: " + author);
        System.out.println("Title: " + title );
        System.out.println("Pages: " + pages );
        
        if (refNumber.equals("") || refNumber.isEmpty()){}
        System.out.println("Ref. Number: " + refNumber);
        
    }
    
    public void setRefNumber(String ref) {
        if(ref.length() >=3){
            refNumber = ref;
        }
        else{
            System.out.println("Error: el numero de referencia del libro" + title + "debe contener al menos 3 caracteres");
        }
        
    
    }
    
    public String getRefNumber(){
        return refNumber;
    }
    
    public void borrow(){
    borrowed++;
    }
    
    public int getBorrowed(){
        return borrowed;
    }
    
    

    // Add the methods here ...
}
